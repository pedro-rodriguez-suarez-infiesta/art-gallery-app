import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'',
    redirectTo: 'home', pathMatch:'full',
  },
  {
    path:'/home',
    loadChildren: () => import ('./pages/home/home.module').then((m) => m.HomeModule)
  },
  {
    path:'about',
    loadChildren: () => import ('./pages/about/about.module').then((m) => m.AboutModule)
  },
  {
    path:'gallery',
    loadChildren: () => import ('./pages/gallery/gallery.module').then((m) => m.GalleryModule)
  },
  {
    path:'form',
    loadChildren: () => import ('./pages/form/form.module').then((m) => m.FormModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
